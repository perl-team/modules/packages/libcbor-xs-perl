libcbor-xs-perl (1.87-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.87.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Thu, 28 Sep 2023 19:17:20 +0200

libcbor-xs-perl (1.86-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.86.

 -- gregor herrmann <gregoa@debian.org>  Mon, 08 Nov 2021 18:27:48 +0100

libcbor-xs-perl (1.85-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.85.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Oct 2021 14:46:42 +0200

libcbor-xs-perl (1.84-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.84.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Fri, 22 Oct 2021 23:10:37 +0200

libcbor-xs-perl (1.83-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.83.

 -- gregor herrmann <gregoa@debian.org>  Fri, 11 Dec 2020 03:05:21 +0100

libcbor-xs-perl (1.82-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.
  * Import upstream version 1.82.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Dec 2020 21:57:34 +0100

libcbor-xs-perl (1.71-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * New upstream release.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Sun, 25 Nov 2018 17:21:06 +0100

libcbor-xs-perl (1.70-1) unstable; urgency=medium

  * New upstream version 1.70
  * Declare compliance with Debian Policy 4.0.1 (no changes)
  * Bump debhelper compatibility level to 10

 -- Nick Morrott <knowledgejunkie@gmail.com>  Mon, 14 Aug 2017 13:24:07 +0100

libcbor-xs-perl (1.60-1) unstable; urgency=medium

  * Import upstream version 1.6

 -- Nick Morrott <knowledgejunkie@gmail.com>  Wed, 21 Dec 2016 19:54:18 +0000

libcbor-xs-perl (1.50-1) unstable; urgency=medium

  * New upstream release.
  * Add uversionmangle, upstream went from 1.41 to 1.5.
  * Remove patch spelling-error-in-manpage (applied upstream).
  * Standards-Version 3.9.8 (no changes required).
  * Enable bindnow flag for ELF binary

 -- Nick Morrott <knowledgejunkie@gmail.com>  Thu, 26 May 2016 19:12:47 +0100

libcbor-xs-perl (1.41-1) unstable; urgency=low

  * Initial Release. (Closes: #818859)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Sat, 26 Mar 2016 16:55:24 +0000
